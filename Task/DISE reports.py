import yagmail
import pandas as pd
import pyodbc as db
import datetime
import openpyxl
import xlsxwriter
import os


today = datetime.datetime.today().strftime("%b_%Y")
t = str(today)

Today = datetime.datetime.today().strftime("%B %Y")
T = str(Today)
directorio = f"G:/Shared drives/DISE automation/Reportes/_{t}_DIME"
try:
    os.mkdir(directorio)
except OSError:
    print("La creación del directorio %s falló" % directorio)
else:
    print("Se ha creado el directorio: %s " % directorio)
df = pd.read_excel(fr"G:\Shared drives\DISE automation\Reportes\_Dime_client_id.xlsx")
for data in df.index:
    # print(f'va en la vuelta {data}')
    sid = df['3_DIGIT_CODE'][data]
    client = df['CUSTOMER'][data]
    datas = data + 1
    direction = f'G:\Shared drives\DISE automation\Reportes\_{t}_DIME'
    file_name_convention = f'{datas}_{client}_{sid}_CLARIFY INVENTORY_{t}'
    big_query = f"""
    SELECT
        cus.NAME AS CUST_NAME,
        sit.X_EXTERNAL_ID2 AS BILLING_SITE_ID,
        sit.SITE_ID,
        sit.NAME AS SITE_NAME,
        spt.INSTANCE_NAME,
        spt.X_ACTIVATION_DATE AS ACTIVATION_DATE,
        spt.PART_STATUS,
        spt.SERIAL_NO,
        prt.DESCRIPTION,
        prt.X_VENDOR AS VENDOR,
        prt.PART_NUMBER,
        prt.PART_TYPE,
        spt.X_IP_ADDRESS AS IP_ADDRESS,
        spt.X_LAN_IP_ADDRESS AS LAN_IP_ADDRESS,
        spt.X_INTERNAL_IP_ADDR AS INTERNAL_IP_ADDRESS,
        spt.X_NAT_IP_ADDRESS AS NAT_IP_ADDRESS,
        adr.ADDRESS,
        adr.ADDRESS_2,
        adr.CITY,
        adr.STATE,
        adr.ZIPCODE,
        cty.NAME AS COUNTRY,
        spt.X_CAT01 AS SERVICE_LEVEL,
        spt.X_CAT19 AS SERVICE_TYPE,
        cus.X_PARTNER AS PARTNER,
        spt.X_COMMENTS2 AS FUNCTIONAL_DESC,
        spt.X_ASSIGNMENT_GROUP AS ASSIGNMENT_GROUP
    FROM
        oldw.TABLE_SITE cus
    JOIN oldw.TABLE_SITE sit ON
        (cus.OBJID = sit.CHILD_SITE2SITE)
    LEFT OUTER JOIN oldw.TABLE_SITE_PART spt ON
        (sit.OBJID = spt.ALL_SITE_PART2SITE)
    LEFT OUTER JOIN oldw.TABLE_MOD_LEVEL lvl ON
        (lvl.OBJID = spt.SITE_PART2PART_INFO)
    LEFT OUTER JOIN oldw.TABLE_PART_NUM prt ON
        (prt.OBJID = lvl.PART_INFO2PART_NUM)
    LEFT OUTER JOIN oldw.TABLE_ADDRESS adr ON
        (adr.OBJID = sit.CUST_PRIMADDR2ADDRESS)
    LEFT OUTER JOIN oldw.TABLE_COUNTRY cty ON
        (cty.OBJID = adr.ADDRESS2COUNTRY)
    WHERE
        spt.LEVEL_TO_PART <> 999
        AND sit.STATUS = 0
        AND cus.X_REGIONAL_NOC = 'NA_MANSFIELD_USA'
        AND cus.STATUS = 0
        AND spt.PART_STATUS <> 'Not Activated'
        AND cus.X_IS_CUSTOMER = 1
        AND prt.PART_TYPE NOT LIKE 'Application%'
        AND cus.SITE_ID = '{sid}'
    ORDER BY
        cus.NAME,
        sit.SITE_ID,
        spt.INSTANCE_NAME
    """
    cnxn = db.connect('DSN=DW_WORLD;PWD=wdata1')
    dfi = pd.read_sql_query(big_query, cnxn)
    cnxn.close()
    cst = dfi.CUST_NAME
    var = str(cst)
    pr = var.split('(')
    vacio = pr[0]
    if vacio == 'Series':
        #CSV
        print('------------------------ ESTE ESTA VACIO ------------------------')
        dfi.to_excel(fr"{direction}\Empty_{file_name_convention}.xlsx", index=None, header=True,startcol=1)  # cambiar a ruta local
        chomp_big_query = pd.read_excel(fr"{direction}\Empty_{file_name_convention}.xlsx").groupby('CUST_NAME')  # read the file created by sql
        chomp_big_query.apply(lambda x: x.to_excel(r'c:\temp\{}.xlsx'.format(x.name), index=None))  # SPLIT DATAFRAME by CUST NAME
    else:
        # CSV
        print('------------------------ ESTE CONTIENE INFORMACION ------------------------')
        dfi.to_excel(fr"{direction}\_{file_name_convention}.xlsx", index = None, header=True, startcol=1)  # cambiar a ruta local
        chomp_big_query = pd.read_excel(fr"{direction}\_{file_name_convention}.xlsx").groupby('CUST_NAME')  # read the file created by sql
        chomp_big_query.apply(lambda x: x.to_excel(r'c:\temp\{}.xlsx'.format(x.name), index=None))  # SPLIT DATAFRAME by CUST NAME
        # writing values to cells
        dre = pd.read_excel(fr"{direction}\_{file_name_convention}.xlsx")
        wb = openpyxl.load_workbook(filename=fr"{direction}\_{file_name_convention}.xlsx")
        i = 2
        for info in dre.index:
            site_id = dre['SITE_ID'][info]
            cust_name = dre['CUST_NAME'][info]
            pr = site_id.split('_')
            ATGA = pr[0]
            name = 'GENERIC SITE - NOT BILLABLE'
            if str(ATGA) == 'ATGA':
                print(f'site_id => {site_id}')
                print(f'cust_name => {cust_name}')
                sheet = wb.active
                c1 = sheet.cell(row=i, column=1)
                c1.value = f"{name}"
            if str(ATGA) == 'ATG':
                print(f'site_id => {site_id}')
                print(f'cust_name => {cust_name}')
                sheet = wb.active
                c1 = sheet.cell(row=i, column=1)
                c1.value = f"{name}"
            i = i + 1
        wb.save(fr"{direction}\_{file_name_convention}.xlsx")
    q = datetime.datetime.now()
    print(q)
    print(' ')

"""SEND BY EMAIL ONCE IS ALL COMPELTED WITH ALL 44 GROUPS """
print('mail time')
subject_text = "DISE_report " + T
el_texto = f""" <p>Good morning ,</p>
<p>I hope this email finds you well.</p>
<p>You can find the DISE reports for {T} en the next direction, I hope it helps you and your organization.&nbsp;</p>
<p>https://drive.google.com/drive/folders/1eNE4WoZ7FbEW54nBgenX3bHPeRGZmE-q &nbsp;</p>
<p>&nbsp;</p>
<p>Have an excellent day!&nbsp;</p>"""

to1 = "ignacio.dominguez@compucom.com"  # requester could change in the future
to2 = "carlos.delahuerta@compucom.com"  # requester could change in the future
to3 = "irving.arrieta@compucom.com"  # requester could change in the future
to4 = "alfredo.hernandez@compucom.com"  # requester could change in the future

yagmail.SMTP({'automated.notifications@compucom.com': 'Automated Notifications'}, 'zlszulirorlaekxj').send(to=[to1, to2, to3, to4], subject=subject_text, contents=[el_texto])
print('end')


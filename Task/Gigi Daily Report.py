import yagmail
import pandas as pd
import pyodbc as db
import datetime

print('Loading....')
gigi_query = (f"""
SELECT
	(
CASE
		WHEN (pha.ORG_ID) = 2 THEN 'USA'
		ELSE 'CAN'
END) AS BOOK,
	(pv.vendor_name),
	(pv.segment1) AS VENDOR_NUMBER,
	(pha.segment1) AS PO_NUMBER,
	(pha.creation_date) PO_DATE,
	(pha.currency_code) PO_CURRENCY,
	(pha.approved_date) PO_APPROVAL_DATE,
	(att.name) TERMS,
	pla.closed_code AS PO_STATUS,
	(aia.INVOICE_NUM),
	(aia.INVOICE_DATE),
	(aia.INVOICE_AMOUNT),
	(aila.line_number),
	AILA.AMOUNT AS LINE_AMOUNT,
	(
CASE
		WHEN (aipa.CHECK_ID) IS NOT NULL THEN 'PAID'
		ELSE (
	CASE
			WHEN (aha.hold_id) IS NOT NULL
			AND aha.status_flag <> 'R' THEN 'HOLD'
			ELSE 'PAYMENT PENDING'
	END)
END) AS STATUS,
	(
CASE
		WHEN (prha.segment1) IS NOT NULL THEN prha.segment1
		ELSE 'DISTRIBUTION PENDING'
END) AS REQUISITION_#,
	(
CASE
		WHEN (prha.creation_date) IS NOT NULL THEN '' || prha.creation_date || ''
		ELSE 'DISTRIBUTION PENDING'
END) AS REQUISITION_DATE
FROM
	po_headers_all@pfin pha
INNER JOIN ap_suppliers@pfin pv ON
	(pv.vendor_id = pha.vendor_id)
INNER JOIN ap_terms_tl@pfin att ON
	(pha.terms_id = att.term_id
	AND att.LANGUAGE = 'US')
INNER JOIN po_lines_all@pfin pla ON
	(pha.po_header_id = pla.PO_header_id)
INNER JOIN ap.ap_invoice_lines_all@pfin aila ON
	( pla.po_line_id = aila.po_line_id )
LEFT JOIN po_distributions_all@pfin pda ON
	(aila.po_distribution_id = pda.po_distribution_id)
LEFT JOIN po_req_distributions_all@pfin prda ON
	(pda.req_distribution_id = prda.distribution_id)
LEFT JOIN po_requisition_lines_all@pfin prla ON
	(prda.requisition_line_id = prla.requisition_line_id)
LEFT JOIN po_requisition_headers_all@pfin prha ON
	(prla.requisition_header_id = prha.requisition_header_id)
INNER JOIN ap_invoices_all@pfin aia ON
	(aila.invoice_id = aia.invoice_id)
LEFT JOIN ap_invoice_payments_all@pfin aipa ON
	(aia.invoice_id = aipa.invoice_id)
LEFT JOIN ap_holds_all@pfin aha ON
	(aila.invoice_id = aha.line_location_id)
WHERE
	pha.creation_date >= '01/JAN/19'
	AND aila.creation_date >= '01/JAN/19'
	AND aia.creation_date >= '01/JAN/19'
	AND pha.org_id IN (2,
	5)
	AND pha.approved_flag = 'Y'
	AND pha.segment1 LIKE 'B%'
	AND aila.discarded_flag = 'N'
	AND aia.CANCELLED_DATE IS NULL
	AND UPPER(aia.SOURCE) IN ('XLS',
	'EDI',
	'MANUAL INVOICE ENTRY')
	-- AND pha.segment1='BU1065698'     -- query by po_number
	-- AND prha.segment1='50103182'  -- query by requsition_number

	GROUP BY aila.po_line_id,
	pha.PO_header_id,
	pha.terms_id,
	pha.ORG_ID,
	pv.vendor_name,
	(pv.segment1),
	(pha.segment1),
	(pha.creation_date),
	(pha.currency_code),
	(pha.approved_date),
	(att.name),
	pla.closed_code,
	(aia.INVOICE_NUM),
	(aia.INVOICE_DATE),
	(aia.INVOICE_AMOUNT),
	aila.invoice_id,
	AILA.AMOUNT,
	(aila.line_number),
	(aipa.CHECK_ID),
	(aha.hold_id),
	aha.status_flag,
	pda.PO_DISTRIBUTION_ID,
	prha.segment1,
	prha.creation_date
ORDER BY
	(pha.segment1) DESC,
	(pv.segment1) DESC,
	(aia.INVOICE_NUM) DESC
""")
today = datetime.date.today()
t = str(today)
# cnxn = db.connect('DSN=DWPROD.WORLD.IP;PWD=Improvement.01')
cnxn = db.connect('DSN=DW_WORLD;PWD=wdata1')
df = pd.read_sql_query(gigi_query, cnxn)
cnxn.close()
# CSV
df.to_csv(f"""C:\\Users\jd201629\Desktop\Task\Reportes\Gigi\Gigi Report {t}.csv""", index=False)
today = datetime.date.today()
# print(today)
t = str(today)
subject_text = "Gigi daily report " + t
el_texto = f"""
<p>Good morning Gigi,</p>
<p>I hope this email finds you well.</p>
<p>Attached is the daily report for {t}, I hope it helps you and your organization.&nbsp;</p>
<p>&nbsp;</p>
<p>Have an excellent day!&nbsp;</p>
"""

to1 = "gigi.callahan@compucom.com"  # requester could change in the future
to2 = 'luis.castillo@compucom.com'  # change to IT's DL
yagmail.SMTP({'automated.notifications@compucom.com': 'Automated Notifications'}, 'zlszulirorlaekxj').send(
    to=[to1, to2], subject=subject_text,
    contents=[el_texto, f"C:\\Users\jd201629\Desktop\Task\Reportes\Gigi\Gigi Report {t}.csv"])

import csv
import time
import datetime
import yagmail
import pandas as pd
import openpyxl
from openpyxl import Workbook
import xlsxwriter
import xlsxwriter
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as ec

# ||=========================================== GENERAL DATA =============================================||

# create excel
today = datetime.datetime.today().strftime("%Y_%d_%m")
t = str(today)
path = (fr'C:\Users\jd201629\Desktop\Task\Reportes\Treasury_backup\Treasury_backup_file_{t}.xlsx') # la ruta va donde existir el archivo
titles = ["Approvals", "Invoice_or_FileName", "Requester", "Vendor_number", "Amount", "Currency",
          "Date_to_Pay", "Requesters_Manager", "Treasury", "Status"]
legal_titles = ["Approvals", "Invoice_or_FileName", "Requester", "Vendor_number", "Beneficiary_Accnt_IBAN",
                "Bank_ABA_SWIFT", "Amount", "Currency", "Date_to_Pay", "Requesters_Manager", "Treasury", "Status"]
wb = Workbook()
ws = wb.active
ws.title = "Sheet1"
wb.save(filename=fr"{path}")
print('File Succesfully Created')
print(' ')

# ||=========================================== TREASURY SITE LEGAL =============================================||

# chrome data
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome = webdriver.Chrome(options=chrome_options)
chrome.implicitly_wait(1)
legal_sites = ['https://sites.google.com/a/compucom.com/treasury/legal']

# change sheet name
wb = openpyxl.load_workbook(filename=fr"{path}")
ss_sheet = wb['Sheet1']
ss_sheet.title = 'Legal'
data_to_add = []

# start the process of copying Legal data
for site in legal_sites:
    chrome.get(site)
    chrome.maximize_window()
    print('Legal Process Started...')
    print('Start copiying data....')
    try:
        chrome.find_element_by_xpath('/html/body/div/div[2]/div[2]/a').click()
        chrome.find_element_by_xpath('//*[@id="view_container"]/div/div/div[2]/'
                                     'div/div[2]/div/div[1]/div/content/span').click()
    except NoSuchElementException:
        pass
    try:
        time.sleep(2)
        chrome.find_element_by_xpath("//span[contains(text(), 'Continue')]").click()
    except NoSuchElementException:
        pass
    table = chrome.find_element_by_xpath(fr"//*[@id='goog-ws-list-table']")
    i = 0
    rows = 1
    for title in legal_titles:
        data_to_add.append(title.strip())
    for b in range(len(legal_titles)):
        i = i + 1
        temp = data_to_add[b]
        sheet = wb.active
        c1 = sheet.cell(row=rows, column=i)
        c1.value = f"{temp}"
    data_to_add = []
    for row in table.find_elements_by_css_selector('tr')[1:-1]:  # [0] is empty header
        try:
            row.find_element_by_class_name('goog-ws-list-checkbox')
            approval = 'Approved'
        except NoSuchElementException:
            approval = 'Rejected'
        data_to_add.append(approval)
        rows = rows + 1
        temp = data_to_add[0]
        sheet = wb.active
        c1 = sheet.cell(row=rows, column=1)
        c1.value = f"{temp}"
        data_to_add = []
        for data in row.find_elements_by_css_selector('td'):
            data_to_add.append(data.text.strip())
        data_to_add.pop(9)
        largo = len(data_to_add)
        i = 1
        for c in range(largo):
            temp = data_to_add[c]
            sheet = wb.active
            i = i + 1
            c1 = sheet.cell(row=rows, column=i)
            c1.value = f"{temp}"
        data_to_add = []
wb.save(fr'{path}')
chrome.quit()
print('Legal data was copied succesfully')
print(' ')
time.sleep(5)

# ||======================================= TREASURY SITE ACCOUNTS PAYABLE =========================================||

# chrome data
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome = webdriver.Chrome(options=chrome_options)
chrome.implicitly_wait(1)
accounts_payable_sites = ['https://sites.google.com/a/compucom.com/treasury/accounts-payble']

# change sheet name
wb = openpyxl.load_workbook(filename=fr"{path}")
ws1 = wb.create_sheet("Accounts_Payable")
ws1.title = "Accounts_Payable"
wb.save(fr'{path}')
print('Accounts Payable Process Started')
print('start copiying data....')
wb = openpyxl.load_workbook(filename=fr"{path}")
data_to_add = []

# start the process of Accounts Payable copying data
for site in accounts_payable_sites:
    chrome.get(site)
    chrome.maximize_window()
    try:
        chrome.find_element_by_xpath('/html/body/div/div[2]/div[2]/a').click()
        chrome.find_element_by_xpath('//*[@id="view_container"]/div/div/div[2]/'
                                     'div/div[2]/div/div[1]/div/content/span').click()
    except NoSuchElementException:
        pass
    try:
        time.sleep(2)
        chrome.find_element_by_xpath("//span[contains(text(), 'Continue')]").click()
    except NoSuchElementException:
        pass
    table = chrome.find_element_by_xpath(fr"//*[@id='goog-ws-list-table']")
    i = 0
    rows = 1
    for title in titles:
        data_to_add.append(title.strip())
    for b in range(len(titles)):
        i = i + 1
        temp = data_to_add[b]
        sheet = wb.active
        cr = wb["Accounts_Payable"]
        c1 = cr.cell(row=rows, column=i)
        c1.value = f"{temp}"
    data_to_add = []
    for row in table.find_elements_by_css_selector('tr')[1:-1]:  # [0] is empty header
        try:
            row.find_element_by_class_name('goog-ws-list-checkbox')
            approval = 'Approved'
        except NoSuchElementException:
            approval = 'Rejected'
        data_to_add.append(approval)
        rows = rows + 1
        temp = data_to_add[0]
        sheet = wb.active
        cr = wb["Accounts_Payable"]
        c1 = cr.cell(row=rows, column=1)
        c1.value = f"{temp}"
        data_to_add = []
        for data in row.find_elements_by_css_selector('td'):
            data_to_add.append(data.text.strip())
        data_to_add.pop(7)
        largo = len(data_to_add)
        i = 1
        for c in range(largo):
            temp = data_to_add[c]
            sheet = wb.active
            cr = wb["Accounts_Payable"]
            i = i + 1
            c1 = cr.cell(row=rows, column=i)
            c1.value = f"{temp}"
        data_to_add = []
wb.save(fr'{path}')
chrome.quit()
print('Accounts Payable data was copied succesfully')
print(' ')
time.sleep(5)

# ||======================================= TREASURY MEXICO =========================================||

# chrome data
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome = webdriver.Chrome(options=chrome_options)
chrome.implicitly_wait(1)
mexico_sites = ['https://sites.google.com/a/compucom.com/treasury/mexico_old/mexico']

# change sheet name
wb = openpyxl.load_workbook(filename=fr"{path}")
ws1 = wb.create_sheet("Mexico")
ws1.title = "Mexico"
wb.save(fr'{path}')
print('Mexico Process Started')
print('start copiying data....')
wb = openpyxl.load_workbook(filename=fr"{path}")
data_to_add = []

# start the process of Mexico copying data
for site in mexico_sites:
    chrome.get(site)
    chrome.maximize_window()
    try:
        chrome.find_element_by_xpath('/html/body/div/div[2]/div[2]/a').click()
        chrome.find_element_by_xpath('//*[@id="view_container"]/div/div/div[2]/'
                                     'div/div[2]/div/div[1]/div/content/span').click()
    except NoSuchElementException:
        pass
    try:
        time.sleep(2)
        chrome.find_element_by_xpath("//span[contains(text(), 'Continue')]").click()
    except NoSuchElementException:
        pass
    table = chrome.find_element_by_xpath(fr"//*[@id='goog-ws-list-table']")
    i = 0
    rows = 1
    for title in titles:
        data_to_add.append(title.strip())
    for b in range(len(titles)):
        i = i + 1
        temp = data_to_add[b]
        sheet = wb.active
        cr = wb["Mexico"]
        c1 = cr.cell(row=rows, column=i)
        c1.value = f"{temp}"
    data_to_add = []
    for row in table.find_elements_by_css_selector('tr')[1:-1]:  # [0] is empty header
        try:
            row.find_element_by_class_name('goog-ws-list-checkbox')
            approval = 'Approved'
        except NoSuchElementException:
            approval = 'Rejected'
        data_to_add.append(approval)
        rows = rows + 1
        temp = data_to_add[0]
        sheet = wb.active
        cr = wb["Mexico"]
        c1 = cr.cell(row=rows, column=1)
        c1.value = f"{temp}"
        data_to_add = []
        for data in row.find_elements_by_css_selector('td'):
            data_to_add.append(data.text.strip())
        data_to_add.pop(7)
        largo = len(data_to_add)
        i = 1
        for c in range(largo):
            temp = data_to_add[c]
            sheet = wb.active
            cr = wb["Mexico"]
            i = i + 1
            c1 = cr.cell(row=rows, column=i)
            c1.value = f"{temp}"
        data_to_add = []
wb.save(fr'{path}')
chrome.quit()
print('Mexico data was copied succesfully')
print(' ')
time.sleep(5)

# ||======================================= TREASURY INDIA =========================================||

# chrome data
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome = webdriver.Chrome(options=chrome_options)
chrome.implicitly_wait(1)
india_sites = ['https://sites.google.com/a/compucom.com/treasury/india_old/india']

# change sheet name
wb = openpyxl.load_workbook(filename=fr"{path}")
ws1 = wb.create_sheet("India")
ws1.title = "India"
wb.save(fr'{path}')
print('India Process Started')
print('start copiying data....')
wb = openpyxl.load_workbook(filename=fr"{path}")
data_to_add = []

# start the process of India copying data
for site in india_sites:
    chrome.get(site)
    chrome.maximize_window()
    try:
        chrome.find_element_by_xpath('/html/body/div/div[2]/div[2]/a').click()
        chrome.find_element_by_xpath('//*[@id="view_container"]/div/div/div[2]/'
                                     'div/div[2]/div/div[1]/div/content/span').click()
    except NoSuchElementException:
        pass
    try:
        time.sleep(2)
        chrome.find_element_by_xpath("//span[contains(text(), 'Continue')]").click()
    except NoSuchElementException:
        pass
    table = chrome.find_element_by_xpath(fr"//*[@id='goog-ws-list-table']")
    i = 0
    rows = 1
    for title in titles:
        data_to_add.append(title.strip())
    for b in range(len(titles)):
        i = i + 1
        temp = data_to_add[b]
        sheet = wb.active
        cr = wb["India"]
        c1 = cr.cell(row=rows, column=i)
        c1.value = f"{temp}"
    data_to_add = []
    for row in table.find_elements_by_css_selector('tr')[1:-1]:  # [0] is empty header
        try:
            row.find_element_by_class_name('goog-ws-list-checkbox')
            approval = 'Approved'
        except NoSuchElementException:
            approval = 'Rejected'
        data_to_add.append(approval)
        rows = rows + 1
        temp = data_to_add[0]
        sheet = wb.active
        cr = wb["India"]
        c1 = cr.cell(row=rows, column=1)
        c1.value = f"{temp}"
        data_to_add = []
        for data in row.find_elements_by_css_selector('td'):
            data_to_add.append(data.text.strip())
        data_to_add.pop(7)
        largo = len(data_to_add)
        i = 1
        for c in range(largo):
            temp = data_to_add[c]
            sheet = wb.active
            cr = wb["India"]
            i = i + 1
            c1 = cr.cell(row=rows, column=i)
            c1.value = f"{temp}"
        data_to_add = []
wb.save(fr'{path}')
chrome.quit()
print('India data was copied succesfully')
print(' ')
time.sleep(5)

# ||======================================= TREASURY CANADA =========================================||

# chrome data
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome = webdriver.Chrome(options=chrome_options)
chrome.implicitly_wait(1)
canada_sites = ['https://sites.google.com/a/compucom.com/treasury/canada']

# change sheet name
wb = openpyxl.load_workbook(filename=fr"{path}")
ws1 = wb.create_sheet("Canada")
ws1.title = "Canada"
wb.save(fr'{path}')
print('Canada Process Started')
print('start copiying data....')
wb = openpyxl.load_workbook(filename=fr"{path}")
data_to_add = []

# start the process of Canada copying data
for site in canada_sites:
    chrome.get(site)
    chrome.maximize_window()
    try:
        chrome.find_element_by_xpath('/html/body/div/div[2]/div[2]/a').click()
        chrome.find_element_by_xpath('//*[@id="view_container"]/div/div/div[2]/'
                                     'div/div[2]/div/div[1]/div/content/span').click()
    except NoSuchElementException:
        pass
    try:
        time.sleep(2)
        chrome.find_element_by_xpath("//span[contains(text(), 'Continue')]").click()
    except NoSuchElementException:
        pass
    table = chrome.find_element_by_xpath(fr"//*[@id='goog-ws-list-table']")
    i = 0
    rows = 1
    for title in titles:
        data_to_add.append(title.strip())
    for b in range(len(titles)):
        i = i + 1
        temp = data_to_add[b]
        sheet = wb.active
        cr = wb["Canada"]
        c1 = cr.cell(row=rows, column=i)
        c1.value = f"{temp}"
    data_to_add = []
    for row in table.find_elements_by_css_selector('tr')[1:-1]:  # [0] is empty header
        try:
            row.find_element_by_class_name('goog-ws-list-checkbox')
            approval = 'Approved'
        except NoSuchElementException:
            approval = 'Rejected'
        data_to_add.append(approval)
        rows = rows + 1
        temp = data_to_add[0]
        sheet = wb.active
        cr = wb["Canada"]
        c1 = cr.cell(row=rows, column=1)
        c1.value = f"{temp}"
        data_to_add = []
        for data in row.find_elements_by_css_selector('td'):
            data_to_add.append(data.text.strip())
        data_to_add.pop(7)
        largo = len(data_to_add)
        i = 1
        for c in range(largo):
            temp = data_to_add[c]
            sheet = wb.active
            cr = wb["Canada"]
            i = i + 1
            c1 = cr.cell(row=rows, column=i)
            c1.value = f"{temp}"
        data_to_add = []
wb.save(fr'{path}')
chrome.quit()
print('Canada data was copied succesfully')
print(' ')
time.sleep(5)

# ||======================================= TREASURY COSTA RICA =========================================||

# chrome data
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome = webdriver.Chrome(options=chrome_options)
chrome.implicitly_wait(1)
costa_rica_sites = ['https://sites.google.com/a/compucom.com/treasury/costarica']

# change sheet name
wb = openpyxl.load_workbook(filename=fr"{path}")
ws1 = wb.create_sheet("Costa_Rica")
ws1.title = "Costa_Rica"
wb.save(fr'{path}')
print('Costa Rica Process Started')
print('start copiying data....')
wb = openpyxl.load_workbook(filename=fr"{path}")
data_to_add = []

# start the process of Costa Rica copying data
for site in costa_rica_sites:
    chrome.get(site)
    chrome.maximize_window()
    try:
        chrome.find_element_by_xpath('/html/body/div/div[2]/div[2]/a').click()
        chrome.find_element_by_xpath('//*[@id="view_container"]/div/div/div[2]/'
                                     'div/div[2]/div/div[1]/div/content/span').click()
    except NoSuchElementException:
        pass
    try:
        time.sleep(2)
        chrome.find_element_by_xpath("//span[contains(text(), 'Continue')]").click()
    except NoSuchElementException:
        pass
    table = chrome.find_element_by_xpath(fr"//*[@id='goog-ws-list-table']")
    i = 0
    rows = 1
    for title in titles:
        data_to_add.append(title.strip())
    for b in range(len(titles)):
        i = i + 1
        temp = data_to_add[b]
        sheet = wb.active
        cr = wb["Costa_Rica"]
        c1 = cr.cell(row=rows, column=i)
        c1.value = f"{temp}"
    data_to_add = []
    for row in table.find_elements_by_css_selector('tr')[1:-1]:  # [0] is empty header
        try:
            row.find_element_by_class_name('goog-ws-list-checkbox')
            approval = 'Approved'
        except NoSuchElementException:
            approval = 'Rejected'
        data_to_add.append(approval)
        rows = rows + 1
        temp = data_to_add[0]
        sheet = wb.active
        cr = wb["Costa_Rica"]
        c1 = cr.cell(row=rows, column=1)
        c1.value = f"{temp}"
        data_to_add = []
        for data in row.find_elements_by_css_selector('td'):
            data_to_add.append(data.text.strip())
        data_to_add.pop(7)
        largo = len(data_to_add)
        i = 1
        for c in range(largo):
            temp = data_to_add[c]
            sheet = wb.active
            cr = wb["Costa_Rica"]
            i = i + 1
            c1 = cr.cell(row=rows, column=i)
            c1.value = f"{temp}"
        data_to_add = []
wb.save(fr'{path}')
chrome.quit()
print('Costa Rica data was copied succesfully')
print(' ')
time.sleep(5)

# ||======================================= TREASURY REPORT MAIL =========================================||

print('Sending Mail Report...')
t = str(today)
subject_text = "Treasury Backup Report " + t
el_texto = f"""
<p>Good morning,</p>
<p>I hope this email finds you well.</p>
<p>Attached is the treasury backup report for {t}, I hope it helps you and your organization.&nbsp;</p>
<p>&nbsp;</p>
<p>Have an excellent day!&nbsp;</p>
"""
today = datetime.datetime.today().strftime("%Y_%d_%m")
t = str(today)
to1 = "ignacio.dominguez@compucom.com"  # requester could change in the future
to2 = 'luis.castillo@compucom.com'  # change to IT's DL
yagmail.SMTP({'automated.notifications@compucom.com': 'Automated Notifications'}, 'zlszulirorlaekxj').send(
    to=[to1, to2], subject=subject_text,
    contents=[el_texto, f"C:\\Users\jd201629\Desktop\Task\Reportes\Treasury_backup\Treasury_backup_file_{t}.xlsx"])

print('The mail was sent successfullyy')
print('Process Finished')

